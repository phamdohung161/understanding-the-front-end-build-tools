const path = require('path');


module.exports = [{
    entry: './tree-shaking/src/index.js',
    mode: 'production',
    output: {
        filename: 'tree-shaking.js',
        path: path.resolve(__dirname, 'dist'),
    },
    resolve: {
        extensions: ['.js', '.jsx'],
    },
    module: {
        rules: [
            {
                test: /\.js|jsx$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                },
            }
        ],
    },
    optimization: {
        usedExports: true,
        mangleExports: true,
    },
}, {
    entry: './tree-shaking/src/index.js',
    mode: 'production',
    output: {
        filename: 'without-tree-shaking.js',
        path: path.resolve(__dirname, 'dist'),
    },
    resolve: {
        extensions: ['.js', '.jsx'],
    },
    module: {
        rules: [
            {
                test: /\.js|jsx$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                },
            },
        ],
    },
    optimization: {
        usedExports: false,
        mangleExports: false,
    },
}];
