(() => {
    var e = {
        63: (e, o, t) => {
            'use strict';
            t.r(o), t.d(o, {util1: () => r, util2: () => n});
            var r = function () {
                console.log('Util 1')
            }, n = function () {
                console.log('Util 2')
            }
        },
    }, o = {};

    function t(r) {
        var n = o[r];
        if (void 0 !== n) return n.exports;
        var l = o[r] = {exports: {}};
        return e[r](l, l.exports, t), l.exports
    }

    t.d = (e, o) => {
        for (var r in o) t.o(o, r) && !t.o(e, r) && Object.defineProperty(e, r, {enumerable: !0, get: o[r]})
    }, t.o = (e, o) => Object.prototype.hasOwnProperty.call(e, o), t.r = e => {
        'undefined' != typeof Symbol && Symbol.toStringTag && Object.defineProperty(e, Symbol.toStringTag, {value: 'Module'}), Object.defineProperty(e, '__esModule', {value: !0})
    }, (0, t(63).util1)()
})();
