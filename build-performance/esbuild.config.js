const path = require('path');
const {CleanWebpackPlugin} = require('clean-webpack-plugin');
const {ESBuildMinifyPlugin} = require('esbuild-loader')
const SpeedMeasurePlugin = require('speed-measure-webpack-plugin');

const smp = new SpeedMeasurePlugin();

module.exports = smp.wrap({
    entry: './build-performance/src/index.js',
    mode: 'production',
    output: {
        filename: '[name].[chunkhash].js',
        chunkFilename: '[name].chunk.js',
        path: path.resolve(__dirname, 'dist-esbuild'),
    },
    resolve: {
        extensions: ['.js'],
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: 'esbuild-loader',
                    options: {
                        target: 'es2015',
                    },
                },
            },
            {
                test: /\.scss$/,
                use: [
                    'style-loader',
                    'fast-css-loader',
                    'fast-sass-loader',
                    {
                        loader: 'esbuild-loader',
                        options: {
                            loader: 'css',
                            minify: true,
                        },
                    },
                ],
            },
        ],
    },
    plugins: [
        new CleanWebpackPlugin(),
    ],
    optimization: {
        minimizer: [
            new ESBuildMinifyPlugin({
                target: 'es2015',
                css: true,
            }),
        ],
    },
});
