import React from 'react';
import loadable from '@loadable/component';
import LazyLoad from 'react-lazyload';

// import Slider from './Slider';

const Slider = loadable(() => import('./Slider'))


import './style.css';
import './bootstrap.scss';

export default function App() {
    const loadJquery = async () => {
        const {default: $} = await import('jquery');
        console.log($);
        alert('done');
    }

    return (
        <>
            <div className="px-4 py-5 my-5 text-center">
                <img className="d-block mx-auto mb-4"
                     src="https://getbootstrap.com/docs/5.2/assets/brand/bootstrap-logo.svg" alt="" width={72}
                     height={57}/>
                <h1 className="display-5 fw-bold">Centered hero</h1>
                <div className="col-lg-6 mx-auto">
                    <p className="lead mb-4">Quickly design and customize responsive mobile-first sites with Bootstrap,
                        the world’s most popular front-end open source toolkit, featuring Sass variables and mixins,
                        responsive grid system, extensive prebuilt components, and powerful JavaScript plugins.</p>
                    <div className="d-grid gap-2 d-sm-flex justify-content-sm-center">
                        <button type="button" className="btn btn-primary btn-lg px-4 gap-3">Primary button</button>
                        <button type="button" className="btn btn-outline-secondary btn-lg px-4">Secondary</button>
                    </div>
                </div>
            </div>


            <div className={'container px-4 py-5'}>
                <h2 className="pb-2 border-bottom">Lazyload chunk based on a event</h2>
                <button className="btn btn-primary" onClick={loadJquery}>Load jQuery</button>
            </div>

            <div className={'container px-4 py-5'}>
                <h2 className="pb-2 border-bottom">Lazyload component / chunk</h2>
                <Slider/>
            </div>


            <div className="container px-4 py-5" id="custom-cards">
                <h2 className="pb-2 border-bottom">Custom cards</h2>
                <div className="row row-cols-1 row-cols-lg-3 align-items-stretch g-4 py-5">
                    <div className="col">
                        <div className="card card-cover h-100 overflow-hidden text-bg-dark rounded-4 shadow-lg"
                             style={{backgroundImage: 'url("unsplash-photo-1.jpg")'}}>
                            <div className="d-flex flex-column h-100 p-5 pb-3 text-white text-shadow-1">
                                <h2 className="pt-5 mt-5 mb-4 display-6 lh-1 fw-bold">Short title, long jacket</h2>
                                <ul className="d-flex list-unstyled mt-auto">
                                    <li className="me-auto">
                                        <img src="https://github.com/twbs.png" alt="Bootstrap" width={32} height={32}
                                             className="rounded-circle border border-white"/>
                                    </li>
                                    <li className="d-flex align-items-center me-3">
                                        <svg className="bi me-2" width="1em" height="1em">
                                            <use xlinkHref="#geo-fill"/>
                                        </svg>
                                        <small>Earth</small>
                                    </li>
                                    <li className="d-flex align-items-center">
                                        <svg className="bi me-2" width="1em" height="1em">
                                            <use xlinkHref="#calendar3"/>
                                        </svg>
                                        <small>3d</small>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div className="col">
                        <div className="card card-cover h-100 overflow-hidden text-bg-dark rounded-4 shadow-lg"
                             style={{backgroundImage: 'url("unsplash-photo-2.jpg")'}}>
                            <div className="d-flex flex-column h-100 p-5 pb-3 text-white text-shadow-1">
                                <h2 className="pt-5 mt-5 mb-4 display-6 lh-1 fw-bold">Much longer title that wraps to
                                    multiple lines</h2>
                                <ul className="d-flex list-unstyled mt-auto">
                                    <li className="me-auto">
                                        <img src="https://github.com/twbs.png" alt="Bootstrap" width={32} height={32}
                                             className="rounded-circle border border-white"/>
                                    </li>
                                    <li className="d-flex align-items-center me-3">
                                        <svg className="bi me-2" width="1em" height="1em">
                                            <use xlinkHref="#geo-fill"/>
                                        </svg>
                                        <small>Pakistan</small>
                                    </li>
                                    <li className="d-flex align-items-center">
                                        <svg className="bi me-2" width="1em" height="1em">
                                            <use xlinkHref="#calendar3"/>
                                        </svg>
                                        <small>4d</small>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div className="col">
                        <div className="card card-cover h-100 overflow-hidden text-bg-dark rounded-4 shadow-lg"
                             style={{backgroundImage: 'url("unsplash-photo-3.jpg")'}}>
                            <div className="d-flex flex-column h-100 p-5 pb-3 text-shadow-1">
                                <h2 className="pt-5 mt-5 mb-4 display-6 lh-1 fw-bold">Another longer title belongs
                                    here</h2>
                                <ul className="d-flex list-unstyled mt-auto">
                                    <li className="me-auto">
                                        <img src="https://github.com/twbs.png" alt="Bootstrap" width={32} height={32}
                                             className="rounded-circle border border-white"/>
                                    </li>
                                    <li className="d-flex align-items-center me-3">
                                        <svg className="bi me-2" width="1em" height="1em">
                                            <use xlinkHref="#geo-fill"/>
                                        </svg>
                                        <small>California</small>
                                    </li>
                                    <li className="d-flex align-items-center">
                                        <svg className="bi me-2" width="1em" height="1em">
                                            <use xlinkHref="#calendar3"/>
                                        </svg>
                                        <small>5d</small>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div className={'container'}>
                <h2 className="pb-2 border-bottom">Lazy render</h2>
                <LazyLoad offset={100}>
                    <footer className="py-3 my-4">
                        <ul className="nav justify-content-center border-bottom pb-3 mb-3">
                            <li className="nav-item"><a href="#" className="nav-link px-2 text-muted">Home</a></li>
                            <li className="nav-item"><a href="#" className="nav-link px-2 text-muted">Features</a></li>
                            <li className="nav-item"><a href="#" className="nav-link px-2 text-muted">Pricing</a></li>
                            <li className="nav-item"><a href="#" className="nav-link px-2 text-muted">FAQs</a></li>
                            <li className="nav-item"><a href="#" className="nav-link px-2 text-muted">About</a></li>
                        </ul>
                        <p className="text-center text-muted">© 2022 Company, Inc</p>
                    </footer>
                </LazyLoad>
            </div>
        </>
    );
}
